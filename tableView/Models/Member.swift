//
//  Member.swift
//  tableView
//
//  Created by Ayu on 6/29/16.
//  Copyright © 2016 a. All rights reserved.
//

import UIKit

class Member: NSObject {
    var fullname:String = ""
    var age: Int = 0
    var avatarURL:String = "https://qiscuss3.s3.amazonaws.com/uploads/673f5fb4ee176da78891db80281ac1aa/logo.png"
    var job:String = ""

}
