//
//  MyListView.swift
//  tableView
//
//  Created by Ayu on 6/28/16.
//  Copyright © 2016 a. All rights reserved.
//

import UIKit

class MyListView: UITableViewController {
    
    let dataCount:Int = 1000
    var data:[Member] = [Member]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "List View"
        self.navigationController?.navigationBar.barTintColor = UIColor.redColor()
        
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MyCellIdentifier") //merah itu naman identifier

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.createDummyData()
    }
    
    
    //saat class di load dengan membuat variable
    //pake let untuk variable yang tidak pernah berubah dalam perjalanannya
    //pake var jika di perjalanan bakalan berubah
    //karena immutable maka pake let
    
    //pake ovveride karena pada class UITableViewCell
    

    
    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createDummyData(){
        for index in 1...self.dataCount {
            let member = Member()
            member.fullname = "member no. \(index)"
            
            let diff = index % 4
            
            switch(diff){
            case 0:
                member.job = "a"
                break
            case 1:
                member.job = "b"
                default:break
            }
            
            member.age = (index % 20) + 10
            self.data.append(member)
        }
        
        self.tableView.reloadData()
    }
    
     // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 150
        
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.dataCount
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let member = self.data[indexPath.row]
        
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MyCellIdentifier", forIndexPath: indexPath)

        //nama identifier harus sama yang dibuat 
        
        
        //membbuat label
        var nameLabel = cell.viewWithTag(333) as? UILabel //memungkinkan data nil
        var jobLabel = cell.viewWithTag(334) as? UILabel
        var avatar = cell.viewWithTag(335) as? UIImageView
        
        cell.backgroundColor = UIColor.whiteColor()
        
        if nameLabel == nil{
            nameLabel = UILabel(frame: CGRectMake (65,5,screenWidth - 50,20)) //55 border text dengan pojok kiri
            nameLabel?.tag = 333
            cell.addSubview(nameLabel!)
            
            jobLabel = UILabel(frame: CGRectMake (65,30,screenWidth - 50, 20) ) //dibawahnya
            jobLabel?.tag = 334
            cell.addSubview(jobLabel!)
            
            avatar = UIImageView(frame: CGRectMake (5,5,55,55) ) //dibawahnya
            jobLabel?.tag = 335
            cell.addSubview(avatar!)
        }
        
        if let urlAvatar = NSURL(string: member.avatarURL){
            if let data = NSData(contentsOfURL: urlAvatar){
                if let image = UIImage(data: data){
                    avatar?.image = image
                }
            }
        }
        //cell.textLabel?.text = "\(member.fullname)"
        
        nameLabel?.text = member.fullname
        jobLabel?.text = member.job

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let newView = UIViewController() //variable baru yang membawa ke controller baru
        newView.title = "My List no. \(indexPath.row + 1)"
        self.navigationController?.pushViewController(newView, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
